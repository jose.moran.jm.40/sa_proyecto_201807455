# Collection User

```json
[
    {
       id: "number",
       name: "string",
       email: "string",
       date_birth: "date", 
       password: "string",
       tipo_usuario: "string",
       two_phases:"boolean",
       phone_number:"string",
       passport_covid:[
           {
               schema_id:"number",
               dosis:[
                   {
                       number:"number",
                       date:"date"
                   },
                   ...
               ]
           }
       ]
    },
    ...
]
```

# Collection Hotels
```json
[
    {
        id: "number",
        name: "string",
        owner_user_id: "id",
        location: "string",
        country: "string",
        capacity: "number",
        rooms: [
            id:"number",
            name: "string",
            beds: "number",
            type: "string",
            price: "decimal",
            capacity:"number"
        ],
        extra_services: [
            {
                id:"number",
                name:"string",
                price:"decimal"
            },
            ...
        ],
        

    },
    ...
```

# Collection Car_rental

```json
[
    {
        id: "number",
        name: "string",
        owner_user_id: "id",
        location: "string",
        country: "string",
        cars:[
            {
                id: "number",
                model:"string",
                color:"string",
                price:"decimal",
                plate: "string",
                type: "string",
                capacity: "string",
                power_unit: "string"
            },
            ...
        ],
        reservations:[
            {
                authorization_token:"number",
                car_id:"number",
                user_id:"number",
                init_date:"date",
                end_date: "date"
            },
            ...
        ]

    },
    ...
]
```

# Collection Fligths
```json
[
    {
        id:"number",
        origin:"string",
        destination:"string",
        price:"decimal",
        capacity:"number",
        current_capacity_available:"number",
        init_date:"date",
        end_date:"date",
        fligths_sold:[
            {
                authorization_token:"number",
                user_id:"number"
            },
            ...
        ]
    },
    ...
]
```

# Collection Review
```json
[
    {
        id:"number",
        type_collection:"string",
        user_id:"number",
        value:"number",
        comment:"string"
    },
    ...
]
```

# Collection Payments
```json
[
    {
        bank_name:"string",
        bank_id: "number",
        authorization_token:"string",
        quantity:"number"
    },
    ...
]
```

# Collection Vaccine_schema
```json
[
    {
        id_schema:"number",
        vaccine_name:"string",
        dosis:"number"
    }
]
```
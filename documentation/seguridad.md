# Seguridad
## JWT
Este es un token que permite propagar de manera segura la identidad de un usuario de un lado a otro, garantizando que únicamente el usuario es quien tenga acceso a los datos que desee, siempre y cuando este tenga los permisos necesarios para accederlos.

Este utiliza un formato JSON y garantizara que nuestra aplicación siempre sepa la identidad de la persona que desea obtener los servicios.

## Two Phases
Este es un método que el usuario tendrá opcional para activar en su cuenta, por medio de este método se enviara un código único temporal para garantizar que sea quien dice ser desde un inicio de sesion.

## SSL
Es una tecnología que nos permite cifrar data por medio de un navegador o servidor, esto garantizara la integridad y la invisibilidad de la data mientras se transfiere de un lado a otro.

## Red Interna
Kubernetes nos provee varios servicios y mediante la configuración de estos se pueden generar ips internas que no son accesibles al público, haciendo que la comunicación entre servicos sea la mas rapida y segura posible.

# Pipelines para los servicios
Se contara con 3 stages los cuales son 
- test 
- prod

el proceso para cada stage sera el siguiente

## Test
1. Construir la imagen del contenedor de docker para ambiente de prueba para servicio
2. Correr el contenedor con las pruebas unitarias de servicio.
3. Invocar el cluster 
4. Aplicar el archivo de deployment
5. Aplicar el archivo de servicios
6. Remover las imagenes de prueba



## Prod
1. Correr el contenedor con las pruebas unitarias de servicio.
2. Invocar el cluster 
3. Aplicar el archivo de deployment
4. Aplicar el archivo de servicios
# Historias de usuarios
- Como usuario quiero registrar usuarios de tipo turista, hotel y renta de autos para poder navegar en el sistema.
- Como administrador quiero poder registrar usuarios tipo turista, hotel, renta de autos y administrador para poder usar el sistema
- Como usuario tipo hotel quiero poder registrar una calendarización de reservaciones para que los usuarios puedan reservar
- Como usuario tipo turista quiero poder realizar búsquedas en cascada de hoteles para poder filtrar los hoteles
- Como usuario tipo turista quiero poder realizar reservaciones en hoteles con tarjeta de crédito para poder reservar con anticipación
- Como usuario tipo renta de autos quiero tener un formulario para crear autos
- Como usuario tipo renta de autos quiero tener una opción de carga masiva para poder cargar muchos autos a la vez
- Como usuario tipo turista quiero poder realizar reservaciones en autos con tarjeta de crédito para poder reservar con anticipación
- Como usuario tipo turista quiero poder crear mi pasaporte COVID para poder viajar a muchos países
- Como usuario quiero poder descargar el pasaporte COVID por medio de QR para poder verificar que un usuario este vacunado
- Como administrador quiero poder crear nuevos esquemas de vacunación para que los usuarios se puedan registrar
- Como administrador quiero poder actualizar los esquemas de vacunación para poder agregar nuevas dosis
- Como administrador quiero tener un formulario para crear vuelos
- Como administrador quiero tener una opción de carga masiva para poder cargar muchos vuelos a la vez
- Como usuario tipo turista quiero poder realizar pagos de vuelos con tarjeta de crédito para poder viajar
- Como usuario quiero poder pagar con tarjeta de crédito para poder pagar desde cualquier sitio
- Como usuario tipo turista quiero poder emitir una calificación del servicio para que otros usuarios puedan ver mi reseña
- Como usuario quiero poder ver las reseñas de otros usuarios para poder elegir con más eficacia


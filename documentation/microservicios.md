# MICROSERVICIOS

## 1. USUARIOS
Este cuenta con toda la gestión de usuarios dividos por un crud con los siguientes lineamientos
- Creación de usuarios
    - Administrador
    - Turista
    - Renta de autos
    - Hotel
- Eliminación de usuarios
    - Solo usuario administrador podrá realizar esta acción
- Modificación de usuarios
- Obtención de cada usuario por un identificador

Además contara con acciones para la gestión de pasaporte COVID para lo que es necesario las siguientes acciones
- Creación de pasaporte COVID por turista
- Creación de esquemas de vacunación
- Modificación de esquemas de vacunación
- Descarga pdf de pasaporte COVID

## 2. HOTELES
Este microservicio será el encargado de gestionar todos los servicios que puede ofrecer un hotel para los cuales se tendrán las siguientes acciones:
- Reservación de habitación
- Creación de calendarización
- Reseña de servicio de hotel

## 3. RENTA DE AUTOS
Este microservicio será el encargado de gestionar todos los servicios que puede ofrecer un usuario tipo renta de autos para los cuales se tendrán las siguientes acciones:
- Reservación de autos
- Renta de autos para el mismo día
- Reseña de servicio por la renta de un auto
- Carga Masiva de autos
- Creación de auto

## 4. VUELOS
Este microservicio será el encargado de gestionar todos los servicios que puede ofrecer el sistema para vuelos con las siguientes acciones:
- Creación de vuelo 
    - Solo ida
    - Ida y vuelta
- Compra de vuelo

## 5. PAGOS
Debido a que todo el sistema estará integrado con pagos de banco mediante una tarjeta de crédito y se reutilizara muchas veces, se ha tomado la decisión de aislarlo y este microservicio tendrá las siguientes acciones:
- Verificación de existencia de tarjeta de crédito
- Pago con tarjeta de crédito

## 6. COLAS
Un requerimiento del sistema es garantizar la disponibilidad del mismo ante cualquier situación, es por ello que se tendrá un microservicio de manejo de estado de cada transacción que se haga con los distintos microservicios que se ofrecen manejando un estado.

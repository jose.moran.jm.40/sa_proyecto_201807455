# MODELO DE BRANCHING
## Gitflow
Esta es una herramienta que nos permite llevar un estricto control de la herramienta de git mediante ramas, una de sus principales caracteristicas es la organizacion para las versiones de codigo, asi como las estandarizacion de nombres y flujo de codificacion.

Debido a la necesidad de crear el sistema por medio de funcionalidades muchas veces aisaladas una de la otra es necesario esta herramienta que nos permite crear las ramas feature las cuales hacen referencia a una funcionalidad en especifico. 

Esta herramienta cuenta con dos ramas principales:
- Master: Rama en la que se tendra la version del sistema a la que el cliente tendra acceso.
- Develop: Rama en la que se tendra la nueva version del sistema, esta rama tendra la caracteristica que se iran incorporando nuevas caracteristicas con el el tiempo de desarrollo las cuales son completamente funcionales.

### Nomenclartura
Para la nomenclatura de ramas se utilizara la siguiente normativa: 
|Tipo|Nombre|Ejemplo|
|------|-------|--------|
|Feature|feature/{{funcionalidad}}|feature/login|
|Hotfix|hotfix/{{error}}|hotfix/loginpassword|
|Bugfix|bugfix/{{erro}}|bugfix/loginpassword|




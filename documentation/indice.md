# PROYECTO
### Software Avanzado
#### José Eduardo Morán Reyes
#### 201807455

## Indicé

- [Metodología a utilizar para el desarrollo del sistema](metodologia.md)
- [Modelo de ramificación](branching.md)
- [Requerimientos Funcionales](requerimientos.md/#REQUERIMIENTOS-FUNCIONALES)
    * [Usuarios](requerimientos.md/#usuarios)
    * [Hoteles](requerimientos.md/#hoteles)
    * [Renta de Autos](requerimientos.md/#renta-de-autos)
    * [Pasaporte COVID](requerimientos.md/#pasaporte-covid)
    * [Banco](requerimientos.md/#banco)
    * [Reseñas](requerimientos.md/#reseñas)
- [Requerimientos No Funcionales](requerimientos.md/#REQUERIMIENTOS-NO-FUNCIONALES)

- [Historias de usuario](historias.md/#historias-de-usuario)
- [Tecnologias](tecnologias.md/#tecnologias)
    - [Backend](tecnologias.md/#backend)
    - [Frontend](tecnologias.md/#frontend)
    - [CI/CD](tecnologias.md/#ci/cd)
    - [Base de datos](tecnologias.md/#base-de-datos)

- [Diagramas de actividades](diagramasActividades.md)
- [Listado y descripción de microservicios](microservicios.md)
- Contratos
    - [Usuarios](./contratos/usuarios.md)
    - [Hoteles](./contratos/hoteles.md)
    - [Renta de autos](./contratos/rentaAutos.md)
- [Modelo de datos](modelodatos.md)
- [Arquitectura](arquitectura.md)
- [Seguridad](seguridad.md)
- [Indicadores para el Negocio](indicadores.md)
- [Documentacion de pipelines](pipelines.md)


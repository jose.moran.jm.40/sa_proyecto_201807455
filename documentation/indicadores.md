# KPI's
## 1. Capacidad de Hoteles registrados
Gracias a este indicador se podran saber el crecimiento que tiene el sistema, ya que en inicio se espera que se registren los hoteles con mayor capacidad y luego se vayan incorporando unos mas pequeños.

## 2. Reseña de clientes por servicio
Este indicador permitira evaluar la calidad de servicios que estan brindando los hoteles y rentas de autos registrados, para establecer mejores estanderes de calidad.

## 3. Tasa de registro de clientes
El registro de nuevos clientes permitira medir el alcance que se esta teniendo con la aplicacion. Se espera que tenga una mayor cantidad de registros por parte movil.

## 4. Trafico por busqueda de hoteles, vuelos y autos
Se espera que estos servicios tengan una mayor demanda y necesiten escalabilidad. El trafico que tengan estos sera critico para saber el desempeño de la implementacion

## 5. Tiempo de sesion por usuario
El tiempo que un usuario navegue por el sistema sera clave para saber si el UX implementado es el esperado.
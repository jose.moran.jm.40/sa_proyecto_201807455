

## BACKEND
### Python
Lenguaje robusto que permite el manejo de múltiples drivers para conexión con base de datos, permitiendo integrar otras tecnologías para el desarrollo de backend que están en continuo desarrollo. 

### Flask
Es un micro framework para python que facilita el desarrollo de aplicaciones web, en este caso específico el uso será para la creación de api.

## Frontend
### Next JS
Framework de trabajo que permite el renderizado desde servidor, integrando la librería de React JS. Este framework tiene características como el renderizado dinamico que hace que la velocidad de la página web aumente en un gran volumen.

## Móvil
### React Native
Framework de trabajo que al igual que Next JS integra la librería de react, esto generara que el código pueda ser reutilizado. Una de las principales características de esta tecnología es que permite desarrollar para multiples sistemas operativos a la vez con la integración de Expo la cual sera utilizada para este proyecto


## CI/CD
### Gitlab Runners
Es un entorno de trabajo que viene integrado con la herramienta gitlab, mediante esta herramienta se pueden realizar tareas automatizadas que permitan medir la calidad del código escrito, así como despliegue de las mismas para entornos de pruebas como lo puede ser QA o también ambientes de producción.

## MICROSERVICIOS
### Docker
Es una herramienta que permitirá la organización y creación de contenedores en donde se permite el aislamiento y facilita la implementación de múltiples aplicaciones en simultaneo. En la arquitectura planteada es una tecnología que tomara mucha relevancia debido a que se crearan microservicios los cuales están contenidos en contendores y la herramienta para gestionarlos será Docker.

### Kubernetes
Es una herramienta para la orquestación de contenedores, mediante esta tecnología podemos aislar aplicaciones completamente una de otra, así como se puede manejar escalabilidad de una manera muy sencilla dependiendo el tráfico que tenga la aplicación. Esta tecnología hará que cada microservicio sea independiente.

# BASE DE DATOS
### MongoDB
Dada la naturaleza de la arquitectura, la base de datos seleccionada es mongodb para el almacenamiento que por estar hecha en un lenguaje como c++ tiene la capacidad de aprovechar muchisimo los recursos aumentando la experiencia final del cliente.


# Login

Ruta: /api/login

Metodo: POST 

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|

### Body

|Nombre|Tipo|
|------|-------|
|email|string|
|password|string (sha2)|

### Respuesta Exitosa
Codigo: 200

Formato: JSON
|Nombre|Tipo|
|------|-------|
|token|string|


# Crear usuario

Ruta: /api/user

Metodo: PUT 

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|

### Body

|Nombre|Tipo|
|------|-------|
|id|number|
|email|string|
|name|string|
|id|number|
|date_birth|string (YYYYMMDD)|
|passport_covid| object|


passport_covid object structure
```json
    {
        schema_id:"number",
        dosis:[
            {
                number:"number",
                date:"date"
            },
            ...
        ]
    }

```

### Respuesta Exitosa
Codigo: 200

# Obtener usuario

Ruta: /api/user/:id

Metodo: GET 

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|

### Respuesta Exitosa
Codigo: 200

Formato: JSON

|Nombre|Tipo|
|------|-------|
|id|number|
|email|string|
|name|string|
|date_birth|string (YYYYMMDD)|
|passaport_covid|object|

# Actualizar usuario

Ruta: /api/user

Metodo: PUT 

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|

### Body

|Nombre|Tipo|
|------|-------|
|id|number|
|email|string|
|name|string|
|date_birth|string (YYYYMMDD)|

### Respuesta Exitosa
Codigo: 200



# Eliminar usuario

Ruta: /api/user

Metodo: DELETE 

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|

### Body

|Nombre|Tipo|
|------|-------|
|id|number|

### Respuesta Exitosa
Codigo: 200

# Errores

|Error|Codigo|
|------|-------|
|Credenciales incorrectad|401|
|Parametro requerido incompleto|422|
|Solicitud incorrecta|400|
|Usuario sin acceso|403|
# Crear renta de autos

Ruta: /api/carrental/

Metodo: POST 

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|


### Body

|Nombre|Tipo|
|------|-------|
|name|string|
|owner_user_id|number|
|location|string|
|country|string|
|cars|object|

cars object structure
```json
[
    {
        id: "number",
        model:"string",
        color:"string",
        price:"decimal",
        plate: "string",
        type: "string",
        capacity: "string",
        power_unit: "string"
    },
    ...
]
```

### Respuesta Exitosa
Codigo: 200

Formato: JSON

|Nombre|Tipo|
|------|-------|
|id|number|


# Actualizar renta de autos

Ruta: /api/carrent/

Metodo: PUT

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|

### Body

|Nombre|Tipo|
|------|-------|
|id|number|
|name|string|
|owner_user_id|number|
|location|string|
|country|string|
|cars|object|

cars object structure
```json
[
    {
        id: "number",
        model:"string",
        color:"string",
        price:"decimal",
        plate: "string",
        type: "string",
        capacity: "string",
        power_unit: "string"
    },
    ...
]
```
### Respuesta Exitosa
Codigo: 200


# Obtener hotel

Ruta: /api/carrental/:id

Metodo: GET 

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|

### Respuesta Exitosa
Codigo: 200

Formato: JSON

|Nombre|Tipo|
|------|-------|
|id|number|
|name|string|
|owner_user_id|number|
|location|string|
|country|string|
|cars|object|

cars object structure
```json
[
    {
        id: "number",
        model:"string",
        color:"string",
        price:"decimal",
        plate: "string",
        type: "string",
        capacity: "string",
        power_unit: "string"
    },
    ...
]
```


# Eliminar Hotel

Ruta: /api/carrental

Metodo: DELETE 

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|

### Body

|Nombre|Tipo|
|------|-------|
|id|number|

### Respuesta Exitosa
Codigo: 200

# Errores

|Error|Codigo|
|------|-------|
|Credenciales incorrectad|401|
|Parametro requerido incompleto|422|
|Solicitud incorrecta|400|
|Usuario sin acceso|403|
# Crear hotel

Ruta: /api/hotel/

Metodo: POST 

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|


### Body

|Nombre|Tipo|
|------|-------|
|name|string|
|owner_user_id|number|
|location|string|
|country|string|
|capacity|number|
|rooms| object|
|extra_services|object|

rooms object structure
```json
[
    name: "string",
    beds: "number",
    type: "string",
    price: "decimal",
    capacity:"number"
]
```

extra_services object structure
```json
[
    {
        name:"string",
        price:"decimal"
    },
    ...
]
```

### Respuesta Exitosa
Codigo: 200

Formato: JSON

|Nombre|Tipo|
|------|-------|
|id|number|


# Actualizar hotel

Ruta: /api/hotel/

Metodo: PUT

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|

### Body

|Nombre|Tipo|
|------|-------|
|id|number|
|name|string|
|owner_user_id|number|
|location|string|
|country|string|
|capacity|number|
|rooms| object|
|extra_services|object|

rooms object structure
```json
[
    name: "string",
    beds: "number",
    type: "string",
    price: "decimal",
    capacity:"number"
]
```

extra_services object structure
```json
[
    {
        name:"string",
        price:"decimal"
    },
    ...
]
```
### Respuesta Exitosa
Codigo: 200


# Obtener hotel

Ruta: /api/hotel/:id

Metodo: GET 

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|

### Respuesta Exitosa
Codigo: 200

Formato: JSON

|Nombre|Tipo|
|------|-------|
|id|number|
|name|string|
|owner_user_id|number|
|location|string|
|country|string|
|capacity|number|
|rooms| object|
|extra_services|object|

rooms object structure
```json
[
    name: "string",
    beds: "number",
    type: "string",
    price: "decimal",
    capacity:"number"
]
```

extra_services object structure
```json
[
    {
        name:"string",
        price:"decimal"
    },
    ...
]
```


# Eliminar Hotel

Ruta: /api/hotel

Metodo: DELETE 

### Headers

|Nombre|Valor|
|------|-------|
|content-type|aplication/json|
|Authorization|Bearer token|

### Body

|Nombre|Tipo|
|------|-------|
|id|number|

### Respuesta Exitosa
Codigo: 200

# Errores

|Error|Codigo|
|------|-------|
|Credenciales incorrectad|401|
|Parametro requerido incompleto|422|
|Solicitud incorrecta|400|
|Usuario sin acceso|403|
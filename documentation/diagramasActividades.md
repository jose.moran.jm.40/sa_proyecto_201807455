# Diagrama de actividades

## Login

![img](./src/img/DALogin.png)

## Calendarizacion de hoteles

![img](./src/img/DACalendarizacion.png)

## Carga masiva de de vuelos

![img](./src/img/DACargamasivaautos.png)

## Compra de vuelos

![img](./src/img/DACompraVuelos.png)

## Filtro de busqueda para hoteles

![img](./src/img/DAFiltrobusqueda.png)

## Descarga de pasaporte covid con QR

![img](./src/img/DAQR.png)

## Registro de nuevo esquema de vacunas

![img](./src/img/DARegistroEsquema.png)

## Registro de usuarios

![img](./src/img/DARegistro.png)

## Registro de renta de autos

![img](./src/img/DARegistroRentaAuto.png)

## Registro de reservacion de hotel

![img](./src/img/DARegistroReserHotel.png)

